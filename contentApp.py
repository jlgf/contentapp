#!/usr/bin/python

#
# Simple HTTP Server
# Jesus M. Gonzalez-Barahona
# jgb @ gsyc.es
# TSAI and SAT subjects (Universidad Rey Juan Carlos)
# September 2010
# September 2009
# Febraury 2022


import socket

# Create a TCP objet socket and bind it to a port
# We bind to 'localhost', therefore only accepts connections from the
# same machine
# Port should be 80, but since it needs root privileges,
# let's use one above 1024

class webapp:

    def __init__(self, host, port):
        mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        mySocket.bind((host, port))

        # Queue a maximum of 5 TCP connection requests

        mySocket.listen(5)
        while True:
            print("Waiting for connections")
            (recvSocket, address) = mySocket.accept()
            print("HTTP request received:")
            received = recvSocket.recv(2048).decode()
            # Se utiliza el decode para pasarlo de bytes a string en ASCII
            print(received)

            # Se divide la respuesta y se selecciona lo que interesa
            pet = self.parse(received)

            # Se procesa la respuesta para devolver los codigos http y html
            http, html = self.process(pet)

            response = "HTTP/1.1" + http + "\r\n\r\n" \
                        + html \
                        + "\r\n"

            recvSocket.send(response.encode('utf-8'))
            recvSocket.close()

    def parse(self, received):
        petition = received.split(' ')[1]
        return petition

    def process(self, resource):
        http_code = "200 OK"
        html_code = "<html><body><h1> Hola mundo </h1></body></html>" \
                   + "\r\n"
        return (http_code, html_code)

class contentApp(webapp):


    def __init__(self, host, port):
        self.resources_dict = ['/', '/hola', '/jose']
        super().__init__(host, port)

    def process(self, resource):
        if resource in self.resources_dict:
            http_code = "200 OK"
            html_code = "<html><body><h1> Hello " + resource + " </h1></body></html>"
        else:
            http_code = "404 Not found"
            html_code =  "<html><body>" \
                + "<img src='https://upload.wikimedia.org/wikipedia/commons/9/9b/404-error-css.png'/></body></html>" \
                + "\r\n"
        return (http_code, html_code)

if __name__ == "__main__":
    testWeb = contentApp('localhost', 1234)